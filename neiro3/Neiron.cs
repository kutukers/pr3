﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neiro3
{
    class Neiron
    {
		static Random rnd = new Random();
		public const int sizeOfVector = neironInArrayWidth* neironInArrayHeight;
        private double[,] Wi ;
		public const int neironsCount = 33;
		private int y;
		double [] sum = new double[neironsCount];
		public const int neironInArrayWidth = 10;
		public const int neironInArrayHeight = 10;
		
		private int error;

        public double GetRandomNumber(double minimum, double maximum)
		{
			return rnd.NextDouble() * (maximum - minimum) + minimum;
		}

		public Neiron(){ }

		public void SetWiForNeiron(int numNeiron)
		{
			Wi = new double[ numNeiron, sizeOfVector+1];
			double wo = GetRandomNumber(0, 1);
			for (int i = 0; i < numNeiron; i++)
			{
				for (int j = 0; j < sizeOfVector; j++)
				{
					Wi[i, j] = new double();
					Wi[i, j] = GetRandomNumber(0, 1);
				}

				Wi[i, sizeOfVector] = wo;
			}
		}
		

    public int OutputSignal(int numNeiron, int[] xi)
		{
			
				sum[numNeiron] = 0;
				for (int j = 0; j <= sizeOfVector; j++)
				{
					sum[numNeiron] += xi[j] * Wi[numNeiron, j];
					
				}
				if (sum[numNeiron] >= 0)
				{
					y = 1;
				}
				else
				{
					y = 0;
				}
            

			return y;
		}

		public int ErrorForNeiron( int di, int y)
		{
			error = 0;
				error = di - y;
			return error;
		}

		public void ChangeWi(int numNeiron, int e,int[] xi)
		{
			for (int i = 0; i <= sizeOfVector; i++)
			{
				if (e > 0)
				{
					Wi[numNeiron, i] = Wi[numNeiron, i] + (0.3 * e * xi[i]);

				}

				if (e < 0)
				{
					Wi[numNeiron, i] = Wi[numNeiron, i] + (0.3 * e * xi[i]);
				}
			}


		}

    }
}
